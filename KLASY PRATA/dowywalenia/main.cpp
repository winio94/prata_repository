#include <iostream>
#include <cstdlib>
#include<ctime>
using namespace std;
void wypisz(int tab[],int n);
int wybor(int tab[], int n);
int wstawianie(int * tab, int n);
int zamiana(int tab[], int n);

int main()
{
    srand (time(NULL));
    int liczba;
    cout<<"Ile chcesz posortowac liczb? ";
    cin>>liczba;
    int * ps = new int [liczba];
    int tab1[liczba];
    int tab2[liczba];
    for(int i=0; i<liczba; i++)
        ps[i]=tab1[i]=tab2[i]=rand();

    cout<<"Liczby do posortowania:\n";
    wypisz(ps,liczba);cout<<endl<<endl;

    cout<<"Sortowanie przez wybor:\n";
    cout<<"ILOSC OPERACJI: "<<wybor(ps,liczba)<<endl;
    cout<<"Posortowane liczby:\n";
    wypisz(ps,liczba);cout<<endl;

    cout<<"Sorotwanie przez wstawianie:\n";
    cout<<"ILOSC OPERACJI: "<<wstawianie(tab1,liczba)<<endl;
    cout<<"Posortowane liczby:\n";
    wypisz(tab1,liczba);cout<<endl;

    cout<<"Sortowanie przez zamiane:\n";
    cout<<"ILOSC OPERACJI: "<<zamiana(tab2,liczba)<<endl;
    cout<<"Posortowane liczby:\n";
    wypisz(tab2,liczba);cout<<endl;

    return 0;
}
void wypisz(int tab[],int n)
{
    for(int i=0; i<n; i++)
        cout<<tab[i]<<" ";
}

int wybor(int tab[], int n)
{
    int count=0;
    for(int j = 0; j < n - 1; j++)
    {
        int pmin = j;
        for(int i = j + 1; i < n; i++){
           count ++;
           if(tab[i] < tab[pmin])    pmin = i;
        }
        swap(tab[pmin], tab[j]);
    }
    return count;
}

int wstawianie(int * tab, int n)
{  int count =0;
    for(int j = n - 2; j >= 0; j--)
    {
        int x = tab[j];
        int i = j + 1;
        while((i < n) && (x > tab[i]))
        {
            count++;
            tab[i - 1] = tab[i];
            i++;
        }
        tab[i - 1] = x;
    }
    return count;
}

int zamiana(int tab[], int n)
{  int count=0;
    for(int j = n - 1; j > 0; j--)
    {
        int p = 1;
        for(int i = 0; i < j; i++)
            if(tab[i] > tab[i + 1])
            {   count++;
                swap(tab[i], tab[i + 1]);
                p = 0;
            }
        if(p) break;
    }
    return count;
}
