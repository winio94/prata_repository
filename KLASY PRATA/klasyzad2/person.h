#ifndef PERSON_H_INCLUDED
#define PERSON_H_INCLUDED

#include<iostream>
#include<string>
using std::string;


class Person
{
   private:
     static const int LIMIT = 256;//stala
     string lname;//nazwisko
     char fname[LIMIT];
   public:
     Person()  { lname = "brak"; fname[0] = '\0'; }//konstruktor domyslny
     Person(const string & ln, const char * fn = "HejTy");//konstruktor

     //ponizsze metody wyswietlają imie i nazwisko
     void Show() const;//w kolejnosci imie nazwisko
     void FormalShow() const;//w kolejnowsci nazwisko imie

};

#endif // PERSON_H_INCLUDED
