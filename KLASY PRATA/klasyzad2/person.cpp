#include "person.h"
#include<cstring>
using std::cout;
using std::endl;

Person::Person(const string & ln, const char * fn)
{
    lname =ln;
    for(int i = 0; i < 10; i++)
    fname[i]=fn[i];
}

void Person::Show() const
{
   cout<<lname<<"\t"<<fname<<endl;
}

void Person::FormalShow() const
{
   cout<<fname<<"\t"<<lname<<endl;
}
