#include "person.h"
using std::cout;
using std::endl;

int main()
{
   Person one;
   Person two("Staszek");
   Person three("Jacek", "Placek");
   one.Show();
   one.FormalShow();
   cout<<endl;
   two.Show();
   two.FormalShow();
   cout<<endl;
   three.Show();
   three.FormalShow();
   cout<<endl;
   return 0;
}
