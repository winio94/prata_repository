#ifndef GOLF_H_INCLUDED
#define GOLF_H_INCLUDED
#include<string>

using namespace std;
class Golf
{
   private:
     string fullname;
     int handicap;
   public:
     Golf();//konstruktor domyslny;
     Golf(const string & fn, int hand );//konstruktor zwykly
     ~Golf();//destruktor
     void Setgolf();
     void Showgolf() const;
};


#endif // GOLF_H_INCLUDED
