#ifndef SALES_H_INCLUDED
#define SALES_H_INCLUDED

#include<iostream>
namespace SALES
{
using std::cout;
using std::cin;
using std::endl;

   const int QUART = 4;
   class Sales
   {
      private:
        double sales[QUART];
        double average;
        double max;
        double min;
      public:
        Sales();//konstruktor domyslny
        Sales(double ar[], int n);//konstrutkor
        ~Sales();//destruktor
        void setSales();
        void showSales() const;
   };
}

#endif // SALES_H_INCLUDED
