#include"sales.h"

namespace SALES
{
   //konstruktor domyslny
   Sales::Sales()
   {
       for(int i = 0;i < QUART; i++)
          sales[i] = 0.0;
       average = 0.0;
       min = 0.0;
       max = 0.0;
   }
   //konstruktor
   Sales::Sales(double ar[], int n)
   {
        for(int i = 0; i < 4; i++)
          sales[i] = ar[i];

       double minimal = ar[0], maximal = ar[0], ave = 0.0;
       for(int i = 0; i < n; i++){
          if(ar[i] < minimal)
            minimal = ar[i];
          else if(ar[i] > maximal)
            maximal = ar[i];
          ave += ar[i];
       }
       ave /= n;
       min = minimal;
       max = maximal;
       average = ave;
   }
   //destruktor
   Sales::~Sales()
   {  }
   //metody
   void Sales::setSales()
   {
       double tab[QUART];
       cout<<"\nPodaj cztery elementy: \n";
       for(int i = 0; i < QUART ; i++){
         cout<<"Element "<< i + 1 <<": ";
         cin>>tab[i];
       }
       *this = Sales(tab ,QUART);
   }

   void Sales::showSales() const
   {
      cout<<"\n\t\tWyswietlanie:\n";
       for(int i = 0; i < QUART; i++){
          cout<<"Element"<< i + 1 <<":  "<<sales[i]<<"\t";
       }
       cout<<"\nElement minimalny: "<<min
           <<"\nElement maksymalny: "<<max
           <<"\nSrednia: "<<average;
   }
}
