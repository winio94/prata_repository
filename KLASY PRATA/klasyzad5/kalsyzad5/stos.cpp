#include"stos.h"
#include<iostream>
#include <cctype>

int main()
{
    using namespace std;
    Stack st;                    // tworzy pusty stos
    char ch;
    customer var;
    static double total = 0;
    cout << "Naciśnij D, aby dodac strukture, \n"
        << "P, aby zdjac strukture ze stosu, lub K, aby zakończyć.\n";
    while (cin >> ch && toupper(ch) != 'K')
    {
        while (cin.get() != '\n')
            continue;
        if (!isalpha(ch))
        {
            cout << '\a';
            continue;
        }
        switch(ch)
        {
            case 'D':
            case 'd': cout << "\nPodaj nazwisko klienta: ";
                      cin>>var.fullname;
                      cout << "Podaj kwote: ";
                      cin >> var.payment;
                      if (st.isfull())
                          cout << "Stos pełen!\n";
                      else
                          st.push(var);
                      break;
            case 'P':
            case 'p': if (st.isempty())
                          cout << "Stos pusty!\n";
                      else {
                          st.pop(var);
                          total += var.payment;
                          cout << "\nZakupy na rachunek: " << var.fullname << " zdjete ze stosu\n"
                               <<"Laczna kwota zakupow: "<< total<<endl;
                      }
                      break;
        }
        cout << "Naciśnij D, aby dodac strukture, \n"
        << "P, aby zdjac strukture ze stosu, lub K, aby zakończyć.\n";
    }
    cout << "Fajrant!\n";
    return 0;
}
