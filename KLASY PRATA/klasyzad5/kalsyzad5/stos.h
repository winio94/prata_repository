#ifndef STOS_H_INCLUDED
#define STOS_H_INCLUDED

struct customer
{
    char fullname[35];
    double payment;
};

class Stack
{
private:
    const static int MAX = 10;
    customer tab[MAX];
    int top;//indeks szczytowego elementu

public:
    Stack();//konstruktor domyslny;
    bool isempty() const;
    bool isfull() const;
    bool push(const customer & str);
    bool pop(customer & str);
};

#endif // STOS_H_INCLUDED
