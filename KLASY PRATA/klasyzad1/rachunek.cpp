#include "rachunek.h"
#include<cctype>
#include<cstdlib>
using std::cout;
using std::endl;

Rachunek::Rachunek()//konstruktor domyslny
{
   nazwisko="brak";
   numer_rachunku="brak";
   saldo=0.0;
}

bool Rachunek::sprawdz(string & num)
{
   int count=0;
   for(int i=0; i < num.length(); i++)
   {
       if(isdigit((int)num[i]))
       count++;
   }
   if(count == num.length()) return true;
   else return false;
}

Rachunek::Rachunek(const string & surename, string number, double sal)//kontruktor
{
   nazwisko=surename;

   if(sprawdz(number))  numer_rachunku = number;
   else cout<<"Podales niewlasciwy numer konta!\nTransakcja przerwana\n";

   if(sal< 0)
      cout<<"Saldo nei moze byc ujemne\nTransakcja przerwana\n";
   else    saldo=sal;
}

Rachunek::~Rachunek()
{

}

void Rachunek::Show() const
{
   cout<<"Pan/Pani "<<nazwisko<<endl
       <<"Numer konta bankowego: "<<numer_rachunku<<endl
       <<"Saldo: "<< saldo<<endl;
}

void Rachunek::Add_money(double n)
{
    if(n < 0)
      cout<<"Nie mozesz przyjmowac kwoty mniejszej od zera!!\nTransakcja przerwana\n";
    else saldo += n;
}

void Rachunek::Spend_money(double n)
{
    if(n < 0)
      cout<<"Nie mozesz wydac kowoty mniejszej od zera!!\nTransakcja przerwana\n";
    else
    {
       if(n > saldo)
         cout<<"Nie mozesz wydac wiecej niz masz na koncie!\nTransakcja przerwana\n";
       else  saldo -= n;
    }
}











