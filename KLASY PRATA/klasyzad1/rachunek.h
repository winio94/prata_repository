#ifndef RACHUNEK_H_INCLUDED
#define RACHUNEK_H_INCLUDED
#include<string>
#include<iostream>
using std::string;
class Rachunek
{
private:
    string nazwisko;
    string numer_rachunku;
    double saldo;
public:
    Rachunek();//konstruktor domyślny
    Rachunek(const string & surename,string number="0.0", double sal=0.0);//konstruktor
    ~Rachunek();//destruktor
    bool sprawdz(string  & num);
    void Show() const;//wyswietlanie obiektu
    void Add_money(double n);
    void Spend_money(double n);
};

#endif // RACHUNEK_H_INCLUDED
