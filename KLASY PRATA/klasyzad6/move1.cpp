#include"move.h"
#include<iostream>

int main()
{
   Move object1(5.12, 17.2);
   Move object2(0.12, 3.2);
   object1.showmove();
   object2.showmove();
   Move object3 = object1.add(object2);
   object3.showmove();
   object3.reset();
   object3.showmove();
}
