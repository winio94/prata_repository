#ifndef MOVE_H_INCLUDED
#define MOVE_H_INCLUDED

class Move
{
   private:
     double x;
     double y;

   public:
     Move(double a = 0, double b = 0);//konstruktor z wartosciami domyslnymi
     void showmove() const;//wyswietla biezace wartosci x i y
     Move add(const Move & m) const;//sumuuje x i y dwoch obektow oraz tworzy nowy obiekt inicjalizowany tymi wartosciami
     void reset( double a = 0, double b = 0);//ustawia x na a, y na b
};

#endif // MOVE_H_INCLUDED
