#include<iostream>
#include"move.h"

using std::cout;
using std::endl;

Move::Move(double a, double b)
{
   x = a;
   y = b;
}
void Move::showmove() const
{
   cout<<"x = "<<x<<",  y = "<<y<<endl;
}
Move Move::add(const Move & m) const
{
   double ex = 0.0 , ey = 0.0;
   ex = m.x + x;
   ey = m.y + y;
   return Move(ex,ey);
}
void Move::reset(double a, double b)
{
   x = a;
   y = b;
}
