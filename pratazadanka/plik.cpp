#include"plik.h"

Dzialanie::Dzialanie()
{
    zmienna = 0.0;
    var = 0.0;
}
Dzialanie::Dzialanie(double p, double q)
{
    zmienna = p;
    var = q;
}
Dzialanie::~Dzialanie()
{}
Dzialanie Dzialanie::operator+(const Dzialanie & l) const
{
    return Dzialanie(zmienna + l.zmienna, var + l.var);
}

Dzialanie Dzialanie::operator-(const Dzialanie & l) const
{
    return Dzialanie(zmienna - l.zmienna, var - l.var);
}

Dzialanie Dzialanie::operator*(const Dzialanie & l) const
{
    return Dzialanie(zmienna * l.zmienna, var * l.var);
}

Dzialanie Dzialanie::operator/(const Dzialanie & l) const
{
    if(l.zmienna == 0 || l.var == 0)
        std::cout<<"Nie mozna dzielic przez zero!!\n";
    else
    {
        return Dzialanie(zmienna / l.zmienna, var / l.var);
    }
}

Dzialanie Dzialanie::operator*(double m) const
{
   return Dzialanie(m * zmienna, m * var);
}

void Dzialanie::Show() const
{
    std::cout<<"zmienna = "<<zmienna<<",  var = "<<var<<"\n";
}

void Dzialanie::Reset(double x, double y)
{
    zmienna = x;
    var = y;
}

//funkcja zaprzyjazniona
/*
Dzialanie operator*(double m, const Dzialanie & l)
{
    Dzialanie var1;
    var1.zmienna = m * l.zmienna;
    var1.var = m * l.var;
    return var1;
}
*/

Dzialanie operator/(double m, const Dzialanie & l)
{
   if(l.zmienna == 0.0 || l.var == 0.0)
      std::cout<<"Nie mozna dzielic przez zero!!\n";
   else
      return Dzialanie(m / l.zmienna, m / l.var);
}

ostream & operator<<(ostream & os, const Dzialanie & l)
{
   os<<"zmienna = "<<l.zmienna<<",  var = "<<l.var<<"\n";
   return os;
}






