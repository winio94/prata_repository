#include"plik.h"

using std::cout;
int main()
{
 double k = 5.0;
   Dzialanie var1(5.4, 2.0);
   Dzialanie var3(10.2, 12.8);
  /* var1.Show();
   var3.Show();
   Dzialanie var4 =  var1 * k;//mnozenie przez zmienna
   var4.Show();
   //var4 = k * var1;  Nie wolno, trzeba uzyc funkcji zaprzyjaznionej
   var4 = var4 * var4;//mnozenie obiektow
   var4.Show();
   var4 = var4 / var3 / var3 + var1;//zlozone wyrazenie
   var4.Show();
   var4 = var4 / var1;//dzielenie przez zero;
   var4.Show();
*/
   //wykorzystanie funkcji zaprzyjaznionej
   //var1.Show();
   cout<<var1;
   var1 = k * var1;
   //var1.Show();
   cout<<var1;
   var1 = var1 * 10.0;
   cout<<var1;

   var1.Reset(5 , 7);
   //var1.Show();
   cout<<var1;

   var1 = k / var1;
   //var1.Show();
   cout<<var1;
   var1 = var1 + var3;
   cout<<var1;

   var1 = var1 - var3;
   cout<<var1;

   return 0;
}
