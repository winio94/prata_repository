#ifndef PLIK_H_INCLUDED
#define PLIK_H_INCLUDED

#include<iostream>
using std::ostream;

class Dzialanie
{
    private:
      double zmienna;
      double var;
    public:
      Dzialanie();//konstruktor domyslny
      Dzialanie(double p,double q);
      ~Dzialanie();
      Dzialanie operator+(const Dzialanie & l) const;
      Dzialanie operator-(const Dzialanie & l) const;
      Dzialanie operator*(const Dzialanie & l) const;
      Dzialanie operator*(double m) const;//mnozenie obiektu przez zmienna
      Dzialanie operator/(const Dzialanie & l) const;
      void Show() const;
      void Reset(double x = 0, double y = 0);

      //funkcja zaprzyjazniona
      friend Dzialanie operator*(double m, const Dzialanie & l)
      {return l * m; }//definicja miejscowa(inline)  //wykorzystane juz istniejacego przeciazenia operatora *
      friend Dzialanie operator/(double m, const Dzialanie & l);
      friend ostream & operator<<(ostream & os, const Dzialanie & l);
};

#endif // PLIK_H_INCLUDED
