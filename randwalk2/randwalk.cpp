// randwalk.cpp -- test klasy Vector
// kompilować z plikiem vect.cpp
#include <iostream>
#include <cstdlib>    // prototypy funkcji rand() i srand()
#include <ctime>      // prototyp funkcji time()
#include<fstream>
#include "vect.h"

int main()
{
    using namespace std;
    ofstream fout;
    fout.open("marsz.txt",ios_base::app);


    using VECTOR::Vector;
    srand(time(0));   // inicjalizacja generatora liczb pseudolosowych
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    cout << "Podaj dystans do przejścia (k, aby zakończyć): ";
    while (cin >> target)
    {
        cout << "Podaj długość kroku: ";
        if (!(cin >> dstep && dstep != 0))
            break;
        fout<<"Dystans do przejscia:"<<target<<", dlugosc korku:"<<dstep<<endl;

        while (result.magval() < target)
        {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            fout<<steps<<": (x, y) = "<<result<<endl;
            result = result + step;
            steps++;

        }
        cout << "Po " << steps << " krokach delikwent "
            "osiągnął położenie:\n";
        fout<< "Po " << steps << " krokach delikwent "
            "osiągnął położenie:\n";
        cout << result << endl;
        fout << result <<endl;//zapisywanie do pliku
        result.polar_mode();
        cout << " czyli\n" << result << endl;
        cout << "Średnia długość kroku pozornego = "
            << result.magval()/steps << endl;
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Podaj dystans do przejścia (k, aby zakończyć): ";
    }
    cout << "Koniec!\n";
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}

