#include <iostream>
#include "golf.h"
using std::cin;
using std::cout;
using std::endl;

int main()
{
  golf g1,g2,g3;
  setgolf(g1,"Winnicki",20);
  showgolf(g1);
  setgolf(g2,"Winnicki",0);
  showgolf(g2);
  setgolf(g2);
  showgolf(g2);
  setgolf(g3);
  showgolf(g3);
  return 0;
}
