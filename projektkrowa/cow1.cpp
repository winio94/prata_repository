#include "cow.h"
#include<iostream>

using namespace std;

int main()
{
    Cow krowa1;
    krowa1.ShowCow();
    Cow krowa2("magda","robotnik", 100);
    krowa2.ShowCow();
    Cow krowa3 = krowa2;
    krowa3.ShowCow();
    Cow krowa4;
    krowa4 = krowa2;
    krowa4.ShowCow();
    Cow krowa5 = "alicjaa";
    krowa5.ShowCow();
    Cow krowa6;
    krowa6 = "marianna";
    krowa6.ShowCow();

    return 0;
}
