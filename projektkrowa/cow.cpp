#include "cow.h"
#include<cstring>
#include<iostream>

using std::cout;
using std::endl;

Cow::Cow()
{
    name[0] = '\0';
    hobby = nullptr;
    weight = 0.0;
}
Cow::Cow(const char * nm, const char * ho, double wt)
{
    strcpy(name, nm);
    hobby = new char[strlen(ho) + 1];
    strcpy(hobby, ho);
    weight = wt;
}
Cow::Cow(const Cow &c)
{
    cout<<"Konstruktor kopiujacy\n";
    strcpy(name, c.name);
    hobby = new char[strlen(c.hobby) + 1];
    strcpy(hobby, c.hobby);
    weight = c.weight;
}
Cow::~Cow()
{
    cout<<"Do widzenia ";
    for(int i = 0 ; i < strlen(name); i++)
        cout<<name[i];
        cout<<endl;
    delete [] hobby;
}
Cow & Cow::operator=(const Cow & c)
{
    cout<<"Operator przypisania\n";

    if(this == &c)
        return *this;
    strcpy(name, c.name);
    delete [] hobby;
    hobby = new char[strlen(c.hobby) + 1];
    strcpy(hobby, c.hobby);
    weight = c.weight;
    return *this;
}

void Cow::ShowCow() const
{
    if(name[0] != '\0')
    {
        cout<<"nazwisko: ";
        for(int i = 0 ; i < strlen(name); i++)
            cout<<name[i];
        cout<<endl<<"hobby: "<<hobby<<endl<<"waga: "<<weight<<endl;
    }
    else cout<<"Krowa nienazwana!\n";

}

