// stone1.cpp -- własne funkcje konwersji
// kompilować z plikiem stonewt1.cpp
#include <iostream>
#include "stonewt1.h"

int main()
{
    using std::cout;
    Stonewt poppins(9,2.8);         // 9 kamieni, 2.8 funta
    double p_wt = poppins;          // konwersja niejawna
    cout << "Konwersja na typ double => ";
    cout << "Poppins: " << p_wt << " funtów.\n";
    cout << "Konwersja na typ int => ";
    cout << "Poppins: " << int (poppins) << " funtów.\n";
    Stonewt mnoz = poppins * 2;
    mnoz.show_stn();
    mnoz = mnoz * 2.0;
    mnoz.show_stn();
    mnoz = mnoz*2.0;
    mnoz.show_stn();
    mnoz = 2.0 * mnoz;
    mnoz.show_stn();
    return 0;
}

