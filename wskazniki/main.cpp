#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;

struct student {
    int index;
    int semester;
    string surname;
    string specialization;
    double average;
};

void menu();
void show_students(const student [], size_t);

void bubble_sort(student [], size_t, bool compare(const student &, const student &));

bool less_average(const student &, const student &);
bool less_surname(const student &, const student &);
bool less_spec(const student &, const student &);

void select_sort(int [], size_t);

ostream & operator<<(ostream &, const student &);
istream & operator>>(istream &, student &);

enum {load_file = 1, sort_surname, sort_spec, sort_avg, show, quit};

int main(){
    srand(time(NULL));

    student * array = NULL;
    int student_count;

    menu();
    int x;
    cin >> x;
    while(x < quit) {
        switch(x){
            case load_file:
            {
                cin.get();
                string filename;
                cin >> filename;
                ifstream fin(filename.c_str());
                if(!fin.is_open()){
                    cerr << "File not found!\n";
                    array = NULL;
                    continue;
                }

                cout << "Data loaded.\n";

                fin >> student_count;
                array = new student[student_count];

                for(int i = 0; i < student_count; ++i)
                    fin >> array[i];

                break;
            }
            case sort_surname:
                bubble_sort(array, student_count, less_surname);
                break;
            case sort_spec:
                bubble_sort(array, student_count, less_spec);
                break;
            case sort_avg:
                bubble_sort(array, student_count, less_average);
                break;
            case show: {
                if(!array) cerr << "No data loaded!\n";
                show_students(array, student_count);
                break;
            }
        }
        menu();
        cin >> x;
    }

    return 0;
}

void show_students(const student array [], size_t size){
    cout << "INDEX   SURNAME  SPECIALIZATION  SEMESTER  AVERAGE\n";
    for(int i = 0; i < size; ++i)
        cout << array[i];
}

void menu(){
    cout << "\n======= MENU ========\n";
    cout << "1. Load from file.\n";
    cout << "2. Sort by surname - ascending.\n";
    cout << "3. Sort by specialization - ascending.\n";
    cout << "4. Sort by average - ascending.\n";
    cout << "5. Show data.\n";
    cout << "6. Quit\n\n";

}

ostream & operator<<(ostream & os, const student & s){
    os.width(5);
    os << s.index << ' ';
    os.width(9);
    os << s.surname << ' ';
    os.width(12);
    os << s.specialization << ' ';
    os.width(8);
    os << s.semester << ' ';
    os.width(7);
    os << s.average << '\n';

    return os;
}

istream & operator>>(istream & is, student & s){
    is >> s.index;
    is.get();
    is >> s.surname >> s.specialization;
    is >> s.semester >> s.average;
    is.get();

    return is;
}

int minimum(int array[], size_t size, int start = 0){
    int min = array[0];
    for(int i = start; i < size; ++i)
        if(array[i] < array[min]) min = i;
    return min;
}

void select_sort(int array[], size_t size){
    for(int i = 0; i < size; ++i){
        int min = minimum(array, size, i + 1);
        std::swap(array[i], array[min]);
    }
}

void bubble_sort(student array[], size_t size,
                   bool compare(const student &, const student &)){
    bool changed;
    do {
        changed = false;
        for(int i = 1; i < size; ++i)
            if(compare(array[i], array[i-1])){
                std::swap(array[i], array[i-1]);
                changed = true;
            }
    } while(changed);
}

bool less_average(const student & a, const student & b){
    return a.average < b.average;
}

bool less_surname(const student & a, const student & b){
    return a.surname < b.surname;
}

bool less_spec(const student & a, const student & b){
    return a.specialization < b.specialization;
}
