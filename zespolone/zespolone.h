#ifndef ZESPOLONE_H_INCLUDED
#define ZESPOLONE_H_INCLUDED

#include<iostream>

class complex
{
    private:
        int rzeczywista;
        int urojona;
    public:
        complex();//konstruktor domyslny
        complex(int rzecz, int uroj);//konstruktor
        ~complex();//destruktor

        //przeciążanie operatorów
        complex operator~();//negacja obiektu
        complex operator+(const complex & c) const;//dodawanie dwoch obiektow
        complex operator-(const complex & c) const;//odejmowanie obiektow
        complex operator*(const complex & c) const;//mnozenie

        //funkcje zaprzyjaźnione
        friend complex operator*(double m, const complex & c);//mnozenie obiektu przez zmienna
        friend std::ostream & operator<<(std::ostream & os, const complex & c);//wyswietlanie za pomocą cout
        friend std::istream & operator>>(std::istream & os, complex & c);

};
#endif // ZESPOLONE_H_INCLUDED

