#include "zespolone.h"
using namespace std;

complex::complex()
{
    rzeczywista = 0;
    urojona = 0;
}

complex::complex(int rzecz, int uroj)
{
    rzeczywista = rzecz;
    urojona = uroj;
}

complex::~complex()
{ }

complex complex::operator~()
{
    return complex(rzeczywista, -urojona);
}

complex complex::operator+(const complex & c) const
{
    return complex(rzeczywista + c.rzeczywista, urojona + c.urojona);
}

complex complex::operator-(const complex & c) const
{
    return complex(rzeczywista - c.rzeczywista, urojona - c.urojona);
}

complex complex::operator*(const complex & c) const
{
    int r, u;
    r = (rzeczywista * c.rzeczywista) - (urojona * c.urojona);
    u = (rzeczywista * c.urojona) + (urojona * c.rzeczywista);
    return complex(r, u);
}

//funkcje zaprzyjaznione

complex operator*(double m, const complex & c)
{
    return complex(m * c.rzeczywista, m * c.urojona);
}

ostream & operator<<(ostream & os, const complex & c)
{
    os << "("<<c.rzeczywista <<","<< c.urojona<<"i)";
    return os;
}

istream & operator>>(istream & os, complex & c)
{
    cout << "składowa rzeczywista: ";
    os >> c.rzeczywista;
    cout << "składowa urojona: ";
    os >> c.urojona;

    return os;
}
