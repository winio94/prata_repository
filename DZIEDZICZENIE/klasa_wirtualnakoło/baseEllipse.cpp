#include "BaseEllipse.h"
#include<iostream>
#include<cmath>

void Figura::Show() const
{
    std::cout<<"Wspolrzedne:\n"
             <<"x: "<<Coox()<<std::endl
             <<"y: "<<Cooy()<<std::endl;
}

void Kolo::Show() const
{
    Figura::Show();
    std::cout<<"Promien kola: "<<Radius()<<std::endl
             <<"Pole kola: "<<Area()<<std::endl;
}

void Elipsa::Show() const
{
    Figura::Show();
    std::cout<<"Promien poziomy elipsy: "<<Radiushorizontal()<<std::endl
             <<"Promien pionowy elipsy: "<<Radiusvertical()<<std::endl
             <<"Pole elipsy: "<<Area()<<std::endl;
}

void Kwadrat::Show() const
{
    Figura::Show();
    std::cout<<"Bok kwadratu: "<<Side()<<std::endl
             <<"Pole kwadratu: "<<Area()<<std::endl;
}

void Prostokat::Show() const
{
    Figura::Show();
    std::cout<<"Dluzszy bok prostokąta: "<<longSide()<<std::endl
             <<"Krotszy bok prostokata: "<<shortSide()<<std::endl
             <<"Pole prostokąta: "<<Area()<<std::endl;
}

double Trojkat::Area() const
{
    double p = Sidea() + Sideb() + Sidec();//polowa obwodu
    return sqrt(p* (p - a)* (p - b) * (p - c));
}

void Trojkat::Show() const
{
    Figura::Show();
    std::cout<<"Dlugosci bokow:\n"
             <<"a: "<<Sidea()<<std::endl
             <<"b: "<<Sideb()<<std::endl
             <<"c: "<<Sidec()<<std::endl
             <<"Pole: "<<Area()<<std::endl;
}
