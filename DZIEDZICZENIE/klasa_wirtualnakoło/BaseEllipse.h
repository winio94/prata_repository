#ifndef BASEELLIPSE_H_INCLUDED
#define BASEELLIPSE_H_INCLUDED

//asbtrakcyjna klasa bazowa
class Figura
{
    private:
    //wspolrzedne srodka figury
        double x;
        double y;
    public:
        Figura(double x0 = 0, double y0 = 0) : x(x0), y(y0) {}
        virtual ~Figura() {}//destruktor wyrtualny
        double Coox() const { return x; }
        double Cooy() const { return y; }
        void Move(double nx, double ny) { x = nx; y = ny;}//przesuniecie figury
        virtual double Area() const = 0;//funkcja czysto wirtualna
        virtual void Show() const = 0;
        virtual void Scale() = 0;
};

class Kolo : public Figura
{
    private:
        double r;//promien
    public:
        Kolo(double x, double y, double radius) : Figura(x,y), r(radius) {}
        double Radius() const { return r; }
        virtual double Area() const { return r * r; }
        virtual void Show() const;
        virtual void Scale(double nr) { r *= nr; }
};

class Elipsa : public Figura
{
    private:
        double a;//promien 1
        double b;// promien 2
    public:
        Elipsa(double x, double y, double sa, double sb) : Figura(x, y), a(sa), b(sb) {}
        double Radiushorizontal() const {return a;}
        double Radiusvertical() const {return b;}
        virtual double Area() const { return a * b; }
        virtual void Show() const;
        virtual void Scale(double nab) { a *= nab; b *= nab; }

};

class Kwadrat : public Figura
{
    private:
        double a;//dlugosc boku
    public:
        Kwadrat(double sx,double sy,double sa) : Figura(sx, sy), a(sa) { }
        double Side() const { return a; }
        virtual double Area() const { return a * a; }
        virtual void Show() const;
        virtual void Scale(double na) { a *= na; }
};

class Prostokat : public Figura
{
    private:
        double a;//bok1
        double b;//bok2
    public:
        Prostokat(double x, double y, double sa, double sb) : Figura(x,y), a(sa), b(sb) {}
        double longSide() const { return a; }
        double shortSide() const {return b; }
        virtual double Area() const { return a * b; }
        virtual void Show() const;
        virtual void Scale(double nab) { a *= nab; b *= nab; }

};

class Trojkat : public Figura
{
    private :
        double a;
        double b;
        double c;
    public:
        Trojkat(double x, double y, double sa, double sb, double sc) : Figura(x, y), a(sa), b(sb), c(sc) {}
        double Sidea() const { return a; }
        double Sideb() const { return b; }
        double Sidec() const { return c; }
        virtual double Area() const;
        virtual void Show() const;
        virtual void Scale(double nab) { a *= nab; b *= nab; }
};




#endif // BASEELLIPSE_H_INCLUDED















