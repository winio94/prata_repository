// brass.cpp -- metody klas do obsługi rachunków
#include <iostream>
#include "brass.h"
using std::cout;
using std::endl;
using std::string;


Brass::Brass(const std::string & s, long an, double bal) : fullName(s), acctNum(an), balance(bal)
{ }

void Brass::Deposit(double amt)
{
    if(amt < 0)
        std::cout<<"Nie mozna wplacic ujemnej kwoty!\noperacja anulowana\n";
    else
        balance += amt;
}

void Brass::Withdraw(double amt)
{
    if(amt < 0)
        std::cout<<"Nie mozna wyplacic ujemnej kwoty!\noperacja anulowana\n";
    else if(amt > balance)
        std::cout<<"Brak wystarczajacych srodkow do wyplacenia takiej kwoty\noperacja anulowana\n";
    else
        balance -= amt;
}

void Brass::ViewAcct() const
{
    std::cout<<"Klient: "<<fullName<<std::endl
             <<"Numer rachunku: "<<acctNum<<std::endl
             <<"Dostepne srodki: "<<balance<<std::endl;
}

BrassPlus::BrassPlus(const std::string & s, long an, double bal, double ml, double r) : Brass(s, an, bal), maxLoan(ml), rate(r)
{
    owesBank = 0.0;
}

BrassPlus::BrassPlus(const Brass & ba, double ml, double r) : Brass(ba), maxLoan(ml), rate(r)
{
    owesBank = 0.0;
}

void BrassPlus::ViewAcct() const
{
    Brass::ViewAcct();
    std::cout<<"Limit debetu: "<<maxLoan<<std::endl
             <<"Zadłuzenie: "<<owesBank<<std::endl;
}

void BrassPlus::Withdraw(double amt)
{
    double bal = Balance();
    if(amt <= bal)
        Brass::Withdraw(amt);
    else if(amt <= bal + maxLoan - owesBank)
    {
        double advance = amt - bal;
        owesBank += advance *(1 + rate);
        std::cout<<"zadluzenie faktyczne: "<<advance <<"zl\n";
        std::cout<<"odsetki: "<<advance * rate<<"zl\n";
        Deposit(advance);
        Brass::Withdraw(amt);
    }
    else
        std::cout<<"Przekroczony limit debetu!\nOperacja anulowana\n";
}



