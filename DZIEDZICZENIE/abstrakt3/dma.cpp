// dma.cpp -- metody klas

#include "dma.h"
#include <cstring>

// metody klasy baseDMA


baseDMA::baseDMA(const char * l, int r)
{
    label = new char[strlen(l)+1];
    strcpy(label, l);
    rating = r;
}
//konstruktor kopiujący klasy bazowej
baseDMA::baseDMA(const baseDMA & rs)
{
    label = new char[strlen(rs.label) + 1];
    strcpy(label,rs.label);
    rating = rs.rating;
}

baseDMA::~baseDMA()
{
    delete [] label;
}
//operator przypsiania
baseDMA & baseDMA::operator=(const baseDMA & rs)
{
    if(this == &rs)
        return *this;

    delete [] label;
    label = new char[strlen(rs.label) + 1];
    strcpy(label, rs.label);
    rating = rs.rating;
    //uzywane do kazkadowych przypisań
    return *this;
}

std::ostream & operator<<(std::ostream & os, const baseDMA & rs)
{
    os<<"Etykieta: "<<rs.label<<std::endl
      <<"Klasa: "<<rs.rating<<std::endl;
    return os;
}


//konstruktor klasy lacksDMA
lacksDMA::lacksDMA(const char * c, const char * l, int r) : baseDMA(l,r)
{
    strncpy(color, c, COL_LEN - 1);
    color[39] = '\0';
}

lacksDMA::lacksDMA(const char * c, const baseDMA & rs) : baseDMA(rs)
{
    strncpy(color, c, COL_LEN - 1);
    color[COL_LEN - 1] = '\0';
}

std::ostream & operator <<(std::ostream & os, const lacksDMA & ls)
{
    //rzutowanie na typ baseDMA &
    //i wywolanie funkcji przeciazone operatora << dla klasy baseDMA
    os << (baseDMA &) ls;
    os << "Kolor: "<<ls.color<<std::endl;
     return os;
}

//konstruktor klasy hasDMA
hasDMA::hasDMA(const char * s, const char * l, int r) : baseDMA(l , r)
{
    style = new char[strlen(s) + 1];
    strcpy(style, s);
}

//konstrutkor kopiujący klasy hasDMA
hasDMA::hasDMA(const char * s, const baseDMA & rs) : baseDMA(rs)
{
    style = new char[strlen(s) + 1];
    strcpy(style , s);
}

hasDMA::hasDMA(const hasDMA & hs) : baseDMA(hs)//konstruktor klasy bazowej moze przyjac jako argument referencje klasy pochodnej
{
    style = new char[strlen(hs.style) + 1];
    strcpy(style, hs.style);
}

hasDMA::~hasDMA()
{
    delete [] style;
}

hasDMA & hasDMA::operator=(const hasDMA & hs)
{
    if(this == &hs)
        return *this;
    //operator przypisania dla składowych label oraz rating
    baseDMA::operator=(hs);//kopiuje dane klasy bazowej

    delete [] style;
    style = new char[strlen(hs.style) + 1];
    strcpy(style , hs.style);

    return *this;
}

std::ostream & operator<<(std::ostream & os, const hasDMA & hs)
{
    os << (baseDMA & )hs;
    os <<"Styl: "<< hs.style<<std::endl;
    return os;
}
