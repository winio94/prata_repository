// usett1.cpp -- korzystanie z klasy bazowej oraz klasy pochodnej
#include <iostream>
#include "tabtenn1.h"

int main ( void )
{
    using std::cout;
    using std::endl;
    TableTennisPlayer * tab[10];
    for(int i = 0; i < 5; i++)
        tab[i] = new TableTennisPlayer("table", "tennis", true);
    for(int j = 5; j < 10; j++)
        tab[j] = new RatedPlayer(100, "rated","player", false);

    for(int k = 0; k < 10; k++)
    {
        tab[k]->Name();
        cout<<tab[k]->HasTable()<<endl;
    }

    cout<<endl<<endl;

    TableTennisPlayer * pt1;
    TableTennisPlayer t1("ref1", "ref1", true);
    TableTennisPlayer & ref1 = t1;
    pt1 = new TableTennisPlayer("pt1", "pt1", true);

    ref1.Name();
    cout<<ref1.HasTable()<<endl;

    t1.Name();
    cout<<t1.HasTable()<<endl;

    pt1->Name();
    cout<<pt1->HasTable()<<endl;

    TableTennisPlayer * pt2;

    //referencja klasy bazowej odnosząca sie do obiektu klasy pochodnej
    RatedPlayer r2(200,"ref2", "ref2", false);
    TableTennisPlayer & ref2 = r2;

    pt2 = new RatedPlayer(300, "pt2", "pt2", false);

    ref2.Name();
    cout<<ref2.HasTable()<<endl;
    r2.Name();
    cout<<r2.HasTable()<<endl;

    pt2->Name();
    cout<<pt2->HasTable()<<endl;

    //inicjalizacja obiektu bazowego obiektem pochdnym za pomocą niejawnego konstruktora kopiującego
    //TableTennisPlayer(const TableTennisPlayer & tp);

    RatedPlayer r3(400, "cos", "cos", true);
    TableTennisPlayer t2(r3);


    TableTennisPlayer t3 = RatedPlayer(500, "proba", "proba", false);
    TableTennisPlayer t4 = r3;


    r3.Name();
    t2.Name();
    cout<<r3.HasTable()<<endl;
    cout<<t2.HasTable()<<endl;
    cout<<r3.Rating()<<endl;
    //cout<<t2.Rating()<<endl;

    cout<<endl<<endl;
    Show(r3);
    Show(t4);

    RatedPlayer r6(1000, "przepisz", "przepisz", true);
    TableTennisPlayer t6;
    t6 = r6;

    Show(r6);
    Show(t6);

    return 0;
}














