// tabtenn1.cpp -- metody klasy bazowej
#include "tabtenn1.h"
#include <iostream>

//lista inicjalizacyjna
TableTennisPlayer::TableTennisPlayer (const string & fn,const string & ln, bool ht ) : firstname(fn),
                                        lastname(ln), hasTable(ht) {}

void TableTennisPlayer::Name() const
{
    std::cout<<firstname<<" "<<lastname<<std::endl;
}

//lista inicjalizacyjna
RatedPlayer::RatedPlayer(unsigned int r,const string & fn ,const string & ln, bool ht) : TableTennisPlayer(fn, ln, ht)
{
    rating = r;
}

RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp) : TableTennisPlayer(tp), rating(r) {}
/*
TableTennisPlayer::TableTennisPlayer(const TableTennisPlayer & tp)
{
    firstname = tp.firstname;
    lastname = tp.lastname;
    hasTable = tp.hasTable;
}

*/
void Show(TableTennisPlayer & tp)
{
    tp.Name();
    std::cout<<tp.HasTable()<<std::endl;
}
