#include"sales.h"

int main()
{
   using SALES::Sales;//dostep do struktury Sales z przestrzeni nazw SALES
   using SALES::setSales;//(Sales & s);
   //using SALES::setSales(Sales & s, const double ar[], int n);
   using SALES::showSales;
   Sales str1,str2,str3;
   double tab[3] {12.4 , 7.3 , 1.6 };
   setSales(str2 , tab, 3);
   setSales(str1);
   showSales(str2);
   showSales(str1);
   setSales(str3);
   showSales(str3);
}
