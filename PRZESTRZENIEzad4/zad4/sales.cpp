#include"sales.h"
namespace SALES
{
    void setSales(Sales & s, const double ar[], int n)
    {
        double min = ar[0], max = ar[0], ave = 0.0;
        for(int i = 0; i < n; i++)
        {
           if(ar[i] < min)
              min = ar[i];
           else if(ar[i] > max)
              max = ar[i];
           ave+=ar[i];
        }
        ave /= n;
        s.sales[0] = min;
        s.min = min;
        s.max = max;
        s.average = ave;
        for(int i = 1; i < 4; i++)
          s.sales[i] = 0;
    }
    void setSales(Sales & s)
    {
       int p = sizeof(s.sales) / sizeof(double);
       cout<<"\nPodaj cztery elementy: \n";

       for(int i = 0; i < p ; i++){
         cout<<"Element "<< i + 1 <<": ";
         cin>>s.sales[i];
       }
       double min = s.sales[0], max = s.sales[0], ave = 0.0;

       for(int i = 0;i < p ; i++)
        {
           if(s.sales[i] < min)
              min = s.sales[i];
           else if(s.sales[i] > max)
              max = s.sales[i];
           ave += s.sales[i];
        }
        ave /= p;
        s.min = min;
        s.max = max;
        s.average = ave;
    }
    void showSales(const Sales & s)
    {
       int p = sizeof(s.sales) / sizeof(double);
       cout<<"\n\t\tWyswietlanie:\n";
       for(int i = 0; i < p; i++){
          cout<<"Element"<< i + 1 <<":  "<<s.sales[i]<<"\t";
       }
       cout<<"\nElement minimalny: "<<s.min
           <<"\nElement maksymalny: "<<s.max
           <<"\nSrednia: "<<s.average;
    }

}
