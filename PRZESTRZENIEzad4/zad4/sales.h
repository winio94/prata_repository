#ifndef SALES_H_INCLUDED
#define SALES_H_INCLUDED
#include<iostream>
namespace SALES
{
using std::cout;
using std::cin;
using std::endl;

   const int QUART = 4;
   struct Sales
   {
       double sales[QUART];
       double average;
       double max;
       double min;
   };
   void setSales(Sales & s, const double ar[], int n);
   void setSales(Sales & s);
   void showSales(const Sales & s);

}

#endif // SALES_H_INCLUDED
